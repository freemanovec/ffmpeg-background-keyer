use rust_embed::RustEmbed;
use std::{
    env::{
        temp_dir,
        args
    },
    fs::{
        File,
        remove_file,
        remove_dir_all,
        create_dir,
        read_dir
    },
    path::Path,
    io::Write,
    process::Command
};
use die::die;
use image::{
    open as image_open
};
use diffimg;

#[derive(RustEmbed)]
#[folder = "src/"]
struct EmbeddedRepository;

struct ImageDifferenceComparison {
    ix: usize,
    difference: f64
}

fn run_ffmpeg_command(ffmpeg_path: &str, mut args: Vec<&str>, take_stderr: bool) -> String {
    // push some default arguments
    args.push("-hide_banner");

    let mut cmd = Command::new(&ffmpeg_path);
    let mut cmdline = String::new();
    cmdline += ffmpeg_path;
    for arg in &args {
        cmdline += " ";
        cmdline += &format!("\"{}\"", &arg);
        cmd.arg(&arg);
    }
    println!("Running '{}'", &cmdline);
    let output = cmd.output().expect(&format!("Command '{}'", &cmdline));
    if !output.status.success() {
        die!("Command '{}' failed", &cmdline);
    }
    let output = if take_stderr {
        output.stderr
    } else {
        output.stdout
    };
    let output = String::from_utf8(output).expect("Unable to read command output");
    output
}

fn main() {

    // extract ffmpeg and prepare it for use
    let ffmpeg_binary_bytes = EmbeddedRepository::get("ffmpeg").expect("Unable to retrive embedded ffmpeg binary");
    let mut ffmpeg_path = temp_dir();
    ffmpeg_path.push(&format!("{}", rand::random::<u64>()));
    println!("Extracting embedded ffmpeg binary to {:?}", &ffmpeg_path);
    if Path::exists(&ffmpeg_path) {
        remove_file(&ffmpeg_path).expect("Unable to remove old ffmpeg binary");
    }
    let mut ffmpeg_file = File::create(&ffmpeg_path).expect("Unable to write ffmpeg binary");
    ffmpeg_file.write(&ffmpeg_binary_bytes).expect("Unable to write ffmpeg binary");
    let ffmpeg_path = ffmpeg_path.as_os_str().to_str().expect("Unable to convert ffmpeg path").to_string();
    Command::new("chmod").arg("+x").arg(&ffmpeg_path).output().expect("Unable to set permissions on ffmpeg binary");

    let output = run_ffmpeg_command(&ffmpeg_path, vec!["-version"], false);
    let ffmpeg_version = output.split_whitespace().nth(2).expect("Unable to read ffmpeg version");
    println!("Detected ffmpeg version: {}", &ffmpeg_version);
    
    // get the number of frames in video
    let args: Vec<String> = args().collect();
    let video_file_path = args.get(1).expect("Unable to read argument");
    println!("Operating on video file {}", &video_file_path);

    let output = run_ffmpeg_command(&ffmpeg_path, vec!["-i", &video_file_path, "-map", "0:v:0", "-c", "copy", "-f", "null", "-"], true);
    let output = output.split("\n").collect::<Vec<&str>>();
    let output = output.iter().rev().nth(2).expect("Unable to read frame count line");
    let output = output.split_whitespace().nth(1).expect("Unable to read frame count");
    let frame_count = output.parse::<u32>().expect("Unable to convert frame count to integer");
    println!("Frame count: {}", &frame_count);

    // extract frames
    let take_count = 20;
    let take_each = frame_count / take_count;
    let mut extracted_frames_path_base = temp_dir();
    extracted_frames_path_base.push(&format!("{}", rand::random::<u64>()));
    let extracted_frames_path_base = extracted_frames_path_base.as_os_str().to_str().expect("OS error").to_string();
    println!("Extracting {} frames (taking every {}th frame) to {}", &take_count, &take_each, &extracted_frames_path_base);
    let mut extracted_frames_path_pattern = String::new();
    extracted_frames_path_pattern.push_str(&extracted_frames_path_base);
    extracted_frames_path_pattern.push_str("/");
    extracted_frames_path_pattern.push_str("%05d.png");
    create_dir(&extracted_frames_path_base).expect("Unable to create temporary frame analysis directory");
    run_ffmpeg_command(&ffmpeg_path, vec!["-i", &video_file_path, "-vf", &format!("select='not(mod(n,{}))'", take_each), "-vsync", "vfr", &extracted_frames_path_pattern], false);

    // make a difference map, each with each, float difference
    println!("Calculating difference scalars");
    let extracted_frame_paths = read_dir(&extracted_frames_path_base).expect("Unable to read temporary frame analysis directory listing");
    let mut extracted_frame_paths_absolute: Vec<String> = Vec::new();
    for path in extracted_frame_paths {
        let dispayable = format!("{}", path.expect("Unable to read path").path().display());
        if dispayable.ends_with(".png") {
            extracted_frame_paths_absolute.push(dispayable);
        }
    }
    let extracted_frame_count = extracted_frame_paths_absolute.len();
    let mut differences: Vec<ImageDifferenceComparison> = Vec::new();
    let first_image = image_open(&Path::new(extracted_frame_paths_absolute.get(0).expect("Unable to get path of the first image"))).expect("Unable to open the first image").to_rgb();
    let height = first_image.height();
    let width = first_image.width();
    for ix_a in 0..extracted_frame_count {
        println!("Calculating difference scalar of image {} out of {}", ix_a + 1, extracted_frame_count);
        let path_a = extracted_frame_paths_absolute.get(ix_a).expect("Iteration error");
        let image_a = image_open(&Path::new(&path_a)).expect("Unable to open image");
        let mut difference: f64 = 0.0;
        for ix_b in 0..extracted_frame_count {
            if ix_a == ix_b {
                continue;
            }
            let path_b = extracted_frame_paths_absolute.get(ix_b).expect("Iteration error");
            let image_b = image_open(&Path::new(&path_b)).expect("Unable to open image");
            let diff = diffimg::diff(&image_a, &image_b);
            difference += diff;
        }
        
        let difference = ImageDifferenceComparison {
            ix: ix_a,
            difference: difference
        };
        differences.push(difference);
    }

    differences.sort_by(|a, b| a.difference.partial_cmp(&b.difference).unwrap_or(std::cmp::Ordering::Equal));
    let most_common_image_ix = differences.first().expect("Unable to find the most common frame").ix;
    let most_common_image_path = extracted_frame_paths_absolute.get(most_common_image_ix).expect("Unable to look up most common frame");

    // generate the difference video
    let mut output_path = String::new();
    output_path += video_file_path;
    output_path += ".greenscreened.mp4";
    println!("Creating output video ({}), this might take a while", &output_path);
    run_ffmpeg_command(&ffmpeg_path, vec!["-i", &most_common_image_path, "-i", &video_file_path, "-filter_complex", &format!("color=#00ff00:size={}x{} [matte];[1:0] format=rgb24, split[mask][video];[0:0][mask] blend=all_mode=difference,curves=m='0/0 .1/0 .2/1 1/1',format=gray,smartblur=1,eq=brightness=30:contrast=3,eq=brightness=50:contrast=2,eq=brightness=-10:contrast=50,smartblur=3,format=rgb24 [mask];[matte][video][mask] maskedmerge,format=rgb24", width, height), "-shortest", "-pix_fmt", "yuv422p", "-c:v", "libx264", "-c:a", "aac", "-q:a", "330", "-y", &output_path], false);

    // cleanup
    println!("Cleaning up");
    remove_dir_all(&extracted_frames_path_base).expect("Unable to cleanup temporary frame analysis directory");
    remove_file(&ffmpeg_path).expect("Unable to cleanup temporary ffmpeg binary");

}
